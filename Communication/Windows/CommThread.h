//---------------------------------------------------------------------------
//TCommThread version 5.0 - 21/10/2003
//Luciano Vieira Koenigkan - e-mail: lucianovk@bol.com.br

#ifndef CommThreadH
#define CommThreadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Registry.hpp>
//---------------------------------------------------------------------------

#define SIZEOFDATA 1000

enum
{
    TC_NOP,
    TC_OPEN,
};


class TCommThread : public TThread
{
private:
        HANDLE DeviceHandle;
        bool Connected;
        AnsiString DeviceName;
        DCB MyDCB;
        DCB OriginalDCB;
        COMMTIMEOUTS MyTimeouts;
        COMMTIMEOUTS OriginalTimeouts;
        int ReceiveQueue;
        int TransmitQueue;
        unsigned long ReadBytes;
        unsigned char *ReceivedData;
        int MaxFails;
        bool Disconnecting;
        int ReceiveInterval;
        int LastError;

        void __fastcall AfterReceiveData();
//        bool TransmitChar(char c);
        int BytesAvailable(void);
        void __fastcall Close();
        void __fastcall Open();
protected:
        void __fastcall Execute();
public:
        __fastcall TCommThread(void(__closure *NewRxMethod)(unsigned char));
        __fastcall TCommThread(void(__closure *NewRxMethod)(unsigned char),void(__closure *NewTxMethod)(unsigned char));
        __fastcall TCommThread();
        __fastcall ~TCommThread();
        void(__closure *RxCallBack)(unsigned char);
        void(__closure *TxCallBack)(unsigned char);
        bool Connect(void);
        bool Disconnect(void);
        bool GetConnected(void);
        bool TransmitData(AnsiString Data);
        bool TransmitData(unsigned char data);
        void SetDeviceName(AnsiString NewDeviceName);
        AnsiString GetDeviceName(void);
        HANDLE GetDeviceHandle(void);
        int GetReceiveQueue(void);
        void SetReceiveQueue(int NewReceiveQueue);
        int GetTransmitQueue(void);
        void SetTransmitQueue(int NewTransmitQueue);
        void SetMaxFails(int NewMaxFails);
        int GetMaxFails(void);
        int GetBaudRate(void);
        void SetBaudRate(int NewBaudRate);
        int GetByteSize(void);
        void SetByteSize(int NewByteSize);
        int GetParity(void);
        void SetParity(int NewParity);
        int GetStopBits(void);
        void SetStopBits(int NewStopBits);
        int GetReadIntervalTimeout(void);
        void SetReadIntervalTimeout(int NewReadIntervalTimeout);
        int GetReadTotalTimeoutMultiplier(void);
        void SetReadTotalTimeoutMultiplier(int NewReadTotalTimeoutMultiplier);
        int GetReadTotalTimeoutConstant(void);
        void SetReadTotalTimeoutConstant(int NewReadTotalTimeoutConstant);
        int GetWriteTotalTimeoutMultiplier(void);
        void SetWriteTotalTimeoutMultiplier(int NewWriteTotalTimeoutMultiplier);
        int GetWriteTotalTimeoutConstant(void);
        void SetWriteTotalTimeoutConstant(int NewWriteTotalTimeoutConstant);
        void SetRxMethod(void(__closure *NewReturnMethod)(unsigned char));
        void SetTxMethod(void(__closure *NewReturnMethod)(unsigned char));
        AnsiString GetAvailableData(void);
        void SetReceiveInterval(int NewReceiveInterval);
        int GetReceiveInterval(void);
        TStringList* TCommThread::GetAvailableDevicesNames(bool IncludeSerial, bool IncludeParallel, TStringList * AvaiableDeviceNames);
        int GetLastError(void) { return(LastError); }
};
//---------------------------------------------------------------------------
#endif
